# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
# SVN $Id$
# $HeadURL$

from guardian import GuardState

from ..masterswitch.states import *
from ..watchdog.states import *
from ..damping.states import *
from ..isolation.states import *

from ..isolation import const as iso_const
from ..watchdog import const as watchdog_const

from ..watchdog import util as watchdog_util
from ..masterswitch import util as masterswitch_util
from ..damping import util as damping_util
from ..isolation import util as isolation_util


class INIT(GuardState):
    def main(self):
        log('determining how to get to defined state...')

        if watchdog_util.is_watchdog_tripped():
            log("WATCHDOG TRIPPED; clearing all ISO and DAMP filters...")
            isolation_util.clear_iso_filters()
            damping_util.clear_damp_filters()

            watchdog_state = watchdog_util.read_watchdog_state()
            if watchdog_state == watchdog_const.WATCHDOG_DEISOLATING_STATE:
                return 'WATCHDOG_TRIPPED_DEISOLATING'
            if watchdog_state == watchdog_const.WATCHDOG_DAMPING_STATE:
                return 'WATCHDOG_TRIPPED_DAMPING'
            if watchdog_state == watchdog_const.WATCHDOG_FULL_SHUTDOWN_STATE:
                return 'WATCHDOG_TRIPPED_FULL_SHUTDOWN'

        if masterswitch_util.is_masterswitch_off():
            log("MASTERSWITCH OFF; clearing all ISO and DAMP filters...")
            isolation_util.clear_iso_filters()
            damping_util.clear_damp_filters()
            return 'MASTERSWITCH_OFF'

        error_message = damping_util.check_if_damping_loops_on()
        if error_message:
            log("DAMP filters not in expected state; clearing all ISO and DAMP filters...")
            isolation_util.clear_iso_filters(ramp=True)
            damping_util.clear_damp_filters()
            return 'READY'

        for control_level in iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS']:
            error_message = isolation_util.check_if_isolation_loops_on(control_level)
            if not error_message:
                log("stage is %s_ISOLATED" % control_level)
                return control_level+'_ISOLATED'

        log("ISO filters not in any known configuration; clearing all ISO filters...")
        isolation_util.clear_iso_filters()
        return 'DAMPED'
